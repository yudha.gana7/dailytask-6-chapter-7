import React, { useState } from "react";

import ExpensesFilter from './ExpensesFilter';
import Card from '../UI/Card';
import './Expenses.css';
import ExpensesList from './ExpensesList';
import ExpensesChart from './ExpensesChart';


const Expenses = (props) => {
  const [filteredYear, setFilteredYear] = useState('2021')

  const filterChangeHandler = selectedYear => {
    setFilteredYear(selectedYear)
  }

  // const filteredExpense = props.items.filter(expense => {
  //   return expense.date.getFullYear().toString() === filteredYear
  // })

  // add showall and by year filter
  var filteredExpenses = props.items;
  if (filteredYear !== 'showAll') {
    filteredExpenses = props.items.filter(item => {
      return (
        item.date.getFullYear() === parseInt(filteredYear)
      )
    });
  }

  return (
    <div>
      <Card className='expenses'>
        <ExpensesFilter selected={filteredYear} onChangeFilter={filterChangeHandler} />
        <ExpensesChart expenses={filteredExpenses} />
        <ExpensesList items={filteredExpenses} />
      </Card>
    </div>
  );
};

export default Expenses;
